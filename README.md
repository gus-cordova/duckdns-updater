# update-duckdns.sh

This script is to update any duckdns record.

```sh
$ ./update-duckdns.sh -h
Usage:

  ./update-duckdns.sh [-h] [-v] [-f] [-n] [-X] [-T TK] [-D DN] [-I IF]

Options:
  -h    Display this message
  -v    Be verbose
  -q    Silence curl output (usually 'OK' acknowledgment line)
  -f    Force update, even if ip addr hasn't changed
  -n    Only display the operations, don't perform them
  -X    Query EXTERNAL ip address instead of local interface addr
  -T TK Use 'TK' as the duckdns token
  -D DN Use 'DN' as the domain name to update
  -I IF Use 'IF' as the local interface's ip address to check

Exits with a success/failure exit code and 'OK'/'ERR' output.

Configuration defaults:
  * Domain: cordg017ws
  * Token: c16b98b1-f1a9-4265-b019-e6afa7ccc07f
  * Interface: enp1s0
  * Silence curl output: no
  * Run verbosely: no
  * Force update: no
  * Use external address: no
```

There are variables in the script to hold the default values for the token, the domain being updated, if the address is external or internal, and the interface used to query the internal ip address (if that's the case).  Those values come out during the `-h` option output.

When run interactively it will read any previously obtained ip address and compare it with the current one, if there's a change **or** if the `-f` flag was provided, it will request a dns record update.

The `-n` option prevents the update from occurring, just logging a message; `-v` displays all the major actions while they're occurring; `-s` hides the `curl` command output from being shown (in case it's running from a crontab for example); and `-X` is to select the external address (when in a private lan) instead of the address of one of the interfaces.